//Animation Logo
$(document).ready(function() {

    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);

    let $logo = $('.logo img');
    let $p = $('.logo p');
    let $accroche = $('.accroche');
    let $date = $('.date');
   
    setTimeout(function() {
        $logo.addClass("is-load");
    },250);
    setTimeout(function() {
        $p.addClass("is-load");
    },1000);
    setTimeout(function() {
        $accroche.addClass("block");
        $date.addClass("block");
    },2000);


    /**
    * Stop animation/transition on window resize
    */
    let resizeTimer;
    window.addEventListener("resize", () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    
    document.body.classList.add("resize-animation-stopper");
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(() => {
        document.body.classList.remove("resize-animation-stopper");
        }, 400);
    });

});